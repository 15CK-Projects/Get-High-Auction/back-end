const productModel = require("../models/product.js");
const biddingEntryModel = require("../models/biddingEntry.js");
const currentEntryModel = require("../models/currentEntry.js");
const userModel = require("../models/user.js");

const ObjectId = require("mongoose").Types.ObjectId;

const Agenda = require("agenda");
const mongoose = require("mongoose");

const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'phwmbfk44s7gu6ww@ethereal.email',
        pass: 'uQxFrNSCBTMmVtVrnA'
    }
});

async function bannedNotificationMail(userId, productId) {
    try {
        let user = await userModel.findById(userId).lean().exec();
        let product = await productModel.findById(productId).lean().exec();

        if (!user || !product) {
            return;
        }

        let bannedBidderOptions = {
            from: "get.high.auction@email.com",
            to: user.email,
            subject: "Banned",
            text: `Đã đã bị banned, không được ra giá đối với sản phẩm: ${product.name}`
        }

        await transporter.sendMail(bannedBidderOptions);

    } catch (error) {
        console.log(error);
    }
}

const productController = {};

productController.list = (req, res) => {
    let searchType = req.query.searchType;
    if (!searchType) {
        res.status(400).json({
            message: "The syntax of query string is invalid"
        });
        return;
    }

    switch (searchType) {
        case "top5-1":
            top5_raGiaNhieuNhat(req, res);
            break;
        case "top5-2":
            top5_giaCaoNhat(req, res);
            break;
        case "top5-3":
            top5_sapKetThuc(req, res);
            break;
        case "custom":
            customSearch(req, res);
            break;
        default:
            res.status(400).json({
                message: "The syntax of query string is invalid"
            });
            break;
    }
};

async function top5_raGiaNhieuNhat(req, res) {
    let currentEntries;
    try {
        currentEntries = await currentEntryModel.find().lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let currentEntryIds = currentEntries.map(currentEntry => currentEntry.product);

    let products;
    try {
        products = await productModel
            .find({ _id: { $in: currentEntryIds } })
            .sort({ biddingCount: -1 })
            .limit(5)
            .populate({ path: "highestBiddingEntry", populate: { path: "bidder", select: "userName reviewPoint" } })
            .lean()
            .exec();
    } catch (error) {
        res.status(500).json({ message: "Interal server error", error: error });
        return;
    }

    res.status(200).json(products);
}

async function top5_giaCaoNhat(req, res) {
    let currentEntries;
    try {
        currentEntries = await currentEntryModel.find().lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let currentEntryIds = currentEntries.map(currentEntry => currentEntry.product);

    let products;
    try {
        products = await productModel
            .find({ _id: { $in: currentEntryIds } })
            .sort({ "highestBiddingEntry.price": -1 })
            .limit(5)
            .populate({ path: "highestBiddingEntry", populate: { path: "bidder", select: "userName reviewPoint" } })
            .lean()
            .exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(products);
}

async function top5_sapKetThuc(req, res) {
    let currentEntries;
    try {
        currentEntries = await currentEntryModel.find().lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let currentEntryIds = currentEntries.map(currentEntry => currentEntry.product);
    let products;
    try {
        products = await productModel
            .find({ _id: { $in: currentEntryIds } })
            .sort({ endTime: 1 })
            .limit(5)
            .populate({ path: "highestBiddingEntry", populate: { path: "bidder", select: "userName reviewPoint" } })
            .lean()
            .exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(products);
}

async function customSearch(req, res) {
    let rawOptions = req.query;
    let searchOptions = {
        name: rawOptions.name,
        category: rawOptions.category,
        sortByEndTime: rawOptions.sortByEndTime,
        sortByPrice: rawOptions.sortByPrice,
        page: Number.parseInt(rawOptions.page),
    };

    const productsPerPage = 8;

    if (Number.isNaN(searchOptions.page) || searchOptions.page <= 0) {
        searchOptions.page = 1;
    }

    if (searchOptions.sortByEndTime !== "ascending" && searchOptions.sortByEndTime !== "descending") {
        searchOptions.sortByEndTime = "none";
    }

    if (searchOptions.sortByPrice !== "ascending" && searchOptions.sortByPrice !== "descending") {
        searchOptions.sortByPrice = "none";
    }

    let query = productModel.find();
    if (searchOptions.name && searchOptions.name !== "") {
        query = query.where({ $text: { $search: searchOptions.name, $language: "none", $caseSensitive: false } });
    }

    if (searchOptions.category && searchOptions.category !== "") {
        query = query.where({ category: searchOptions.category });
    }

    query = query.populate({ path: "highestBiddingEntry", populate: { path: "bidder", select: "userName reviewPoint" } });

    if (searchOptions.sortByEndTime !== "none") {
        query = query.sort({ endTime: searchOptions.sortByEndTime });
    }

    if (searchOptions.sortByPrice !== "none") {
        query = query.sort({ startPrice: searchOptions.sortByPrice, "highestBiddingEntry.price": searchOptions.sortByPrice });
    }

    let products;
    try {
        let skip = productsPerPage * (searchOptions.page - 1);
        let limit = productsPerPage + 1;
        products = await query.skip(skip).limit(limit).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let data = {
        products: products
    };

    if (products.length > productsPerPage) {
        data.hasMore = true;
        products.pop();
    } else {
        data.hasMore = false;
    }

    res.status(200).json(data);
}

productController.show = (req, res) => {
    let id = req.params.id;

    productModel.findById(id)
        .populate({ path: "seller", select: "userName reviewPoint" })
        .populate({ path: "highestBiddingEntry", populate: { path: "bidder", select: "userName reviewPoint" } })
        .exec()
        .then((product) => {
            if (!product) {
                res.status(404).json({
                    message: "No such product"
                });
                return;
            }
            res.json(product);
        })
        .catch((error) => {
            res.status(500).json({ message: "Internal server error", error: error });
        });
};

productController.create = async (req, res) => {
    let userId = req.tokenPayload.userId;
    let data = req.body;

    let product = {
        name: data.name,
        category: data.category,
        images: [],
        description: [{ content: data.description, time: Date.now() }],
        seller: userId,
        startPrice: data.startPrice,
        priceStep: data.priceStep,
        takeoverPrice: data.takeoverPrice,
        startTime: Date.now(),
        endTime: data.endTime,
        timeExtension: data.timeExtension === "true" ? true : false
    };

    for (let file of req.files) {
        product.images.push(file.filename);
    }

    let createdProduct;
    try {
        createdProduct = await productModel.create(product);
    } catch (error) {
        res.status(500).json({ message: "Can't create product", error: error });
        return;
    }

    let createdCurrentEntry;
    try {
        createdCurrentEntry = await currentEntryModel.create({ product: createdProduct._id });
    } catch (error) {
        res.status(500).json({ message: "Can't create product", error: error });
        return;
    }

    let agenda = new Agenda().mongo(mongoose.connection, "agendaJobs");
    // Tạo job delete sản phẩm ra khỏi currentEntry collection khi hết hạn đấu giá.
    agenda.on("ready", () => {
        agenda.schedule(createdProduct.endTime, "DeleteOutOfDateCurrentEntry", { product: createdCurrentEntry.product }, () => {
            console.log("Agenda job is added.");
        });
    });

    res.status(200).json(createdProduct);
};

productController.update = (req, res) => {

};

productController.remove = (req, res) => {

};

productController.listBiddingEntry = async (req, res) => {
    let productId = req.params.id;

    let entries;
    try {
        let currentEntry;
        currentEntry = await currentEntryModel.findOne({ product: productId }).lean().exec();

        // Hot fix!
        let bannedBidders;
        if (currentEntry) {
            bannedBidders = currentEntry.bannedBidders;
        } else {
            bannedBidders = [];
        }

        entries = await biddingEntryModel.find({ product: productId, bidder: { $nin: bannedBidders } }).populate({ path: "bidder", select: "userName" }).sort({ time: "descending" }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(entries);
};

productController.addBidding = async (req, res) => {
    let bidder = req.tokenPayload.userId;
    let productId = req.body.product;
    let price = req.body.price;

    try {
        let foundProduct = await productModel.findById(productId).populate({ path: "highestBiddingEntry" }).exec();
        if (!foundProduct) {
            res.status(404).json({ message: "No such product", type: 1 });
            return;
        }

        let currentEntry = await currentEntryModel.findOne({ product: productId }).lean().exec();
        if (!currentEntry) {
            res.status(404).json({ message: "Product is out of date", type: 2 });
            return;
        }

        let bidderObjectId = ObjectId(bidder); // For compare with ObjectID with ObjectID.
        for (let bannedBidder of currentEntry.bannedBidders) {
            if (bannedBidder.equals(bidderObjectId)) {
                res.status(422).json({ message: "Bidder is banned", type: 1 });
                return;
            }
        }

        let lowerBoundPrice = foundProduct.highestBiddingEntry ? foundProduct.highestBiddingEntry.price : foundProduct.startPrice;
        if (price <= lowerBoundPrice) {
            res.status(422).json({ message: "The price is not higher than start price or the highest price", type: 2 });
            return;
        }

        if (price % foundProduct.priceStep !== 0) {
            res.status(422).json({ message: "The price is invalid (must be multiple of the price step)", type: 3 });
            return;
        }

        let pendingBidding = {
            bidder: bidder,
            price: price,
            time: Date.now()
        };

        currentEntry = await currentEntryModel.findOneAndUpdate(
            { product: productId },
            { $push: { pendingBiddings: pendingBidding } },
            { new: true, upsert: true, }
        ).lean().exec();

        let agenda = new Agenda().mongo(mongoose.connection, "agendaJobs");
        agenda.on("ready", () => {
            agenda.now("AutoBidding", { product: productId });
        });
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json({ message: "OK" });
};

productController.addDescription = async (req, res) => {
    let productId = req.params.id;
    let userId = req.tokenPayload.userId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId, "seller").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundProduct) {
        res.status(404).json({ message: "No such product" });
        return;
    }

    if (!foundProduct.seller.equals(ObjectId(userId))) {
        res.status(403).json({ message: "This user is not seller of this product" });
        return;
    }

    let description = {
        time: Date.now(),
        content: req.body.content
    }

    try {
        foundProduct = await productModel.findByIdAndUpdate(productId, { $push: { description: description } }, { upsert: true, new: true }).lean().exec();
    } catch (error) {
        res.status(404).json({ message: "No such product" });
        return;
    }

    res.status(200).json(foundProduct);
}

productController.banHighestBidder = async (req, res) => {
    let sellerId = req.tokenPayload.userId;
    let productId = req.params.id;

    let currentEntry;
    let foundProduct;
    try {
        currentEntry = await currentEntryModel.findOne({ product: productId })
            .populate({ path: "product", select: "seller highestBiddingEntry", populate: { path: "highestBiddingEntry" } })
            .lean().exec();

        if (!currentEntry) {
            res.status(404).json({ message: "Product is out of date" });
            return;
        }

        if (!currentEntry.product.seller.equals(ObjectId(sellerId))) {
            res.status(403).json({ message: "This user is not seller of this product" });
            return;
        }

        if (!currentEntry.product.highestBiddingEntry) {
            res.status(422).json({ message: "This product don't have any bidding entry." });;
            return;
        }

        let bannedBidder = currentEntry.product.highestBiddingEntry.bidder;

        // Nếu như bidder bị banned có pending bidding thì phải xóa.
        // Cập nhật lại danh sách banned.
        currentEntry = await currentEntryModel.findOneAndUpdate({ product: productId }, { $addToSet: { bannedBidders: bannedBidder }, $pull: { pendingBiddings: { bidder: bannedBidder } } }, { upsert: true, new: true }).lean().exec();

        // Tìm highestBiddingEntry mới (bidding entry của người đặt giá cao thứ nhì);
        let newHighestBiddingEntry;
        newHighestBiddingEntry = await biddingEntryModel.findOne({ product: productId, bidder: { $nin: currentEntry.bannedBidders } }).sort({ price: "descending" }).lean().exec();

        // Cập nhật lại highest bidding entry.
        foundProduct = await productModel.findByIdAndUpdate(productId, { $set: { highestBiddingEntry: newHighestBiddingEntry ? newHighestBiddingEntry._id : null } }, { upsert: true, new: true }).lean().exec();

        bannedNotificationMail(bannedBidder, foundProduct._id);
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(foundProduct);
}

module.exports = productController;
