const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const userModel = require("../models/user.js");
const productModel = require("../models/product.js");
const currentEntryModel = require("../models/currentEntry.js");
const biddingEntryModel = require("../models/biddingEntry.js");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'phwmbfk44s7gu6ww@ethereal.email',
        pass: 'uQxFrNSCBTMmVtVrnA'
    }
});

let userController = {};

userController.list = async function (req, res) {
    const userEntriesPerPage = 1;

    let userName = req.query.userName;
    let page = req.query.page;

    let query = userModel.find({ isDeleted: { $ne: true } });

    if (userName && userName !== "") {
        let regex = new RegExp(`${userName}`);
        query = query.where({ userName: regex });
    }

    let users;
    try {
        let skip = userEntriesPerPage * (page - 1);
        let limit = userEntriesPerPage + 1;
        users = await query.skip(skip).limit(limit).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    let data = {
        users: users
    }

    if (users.length > userEntriesPerPage) {
        data.hasMore = true;
        users.pop();
    } else {
        data.hasMore = false;
    }

    res.status(200).json(data);
}

userController.show = async function (req, res) {
    let userId = req.params.id;

    let foundUser;
    try {
        foundUser = await userModel.findById(userId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Error when getting user" });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    res.status(200).json(foundUser);
};

userController.create = function (req, res) {
    // Don't need to implement this method.
}

userController.update = async function (req, res) {
    let userId = req.params.id;

    let data = req.body;
    // Chỉ cho phép update những thông tin cơ bản (nên duyệt trước ở client).
    let userData = {
        userName: data.userName,
        email: data.email,
        lastName: data.lastName,
        firstName: data.firstName,
        address: data.address
    };

    // Không update những thông tin không được gửi lên (nếu update thì thông tin sẽ bị null).
    for (key in userData) {
        if (!userData[key]) {
            delete userData[key];
        }
    }

    let foundUser;
    try {
        foundUser = await userModel.findByIdAndUpdate(userId, { $set: userData }, { new: true, upsert: true, select: "-password -refreshToken" }).exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    };

    res.status(200).json(foundUser);
}

userController.remove = function (req, res) {
    let userId = req.params.id;

    let foundUser;
    try {
        foundUser = userModel.findByIdAndUpdate(userId, { $set: { isDeleted: true } }, { upsert: true, new: true }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
    }

    res.status(200).json(foundUser);
}

userController.changePassword = async function (req, res) {
    let userId = req.params.id;

    let foundUser;
    try {
        foundUser = await userModel.findById(userId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    let oldPassword = req.body.oldPassword;
    let oldPasswordHash = foundUser.password;

    let isGood;
    try {
        isGood = await bcrypt.compare(oldPassword, oldPasswordHash);
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    let isAdmin = (req.tokenPayload.userRoles.indexOf("admin") !== -1);
    if (!isGood && !isAdmin) {
        res.status(401).json({ message: "Wrong old password" });
        return;
    }

    let newPassword = req.body.newPassword;
    let newPasswordHash;
    try {
        newPasswordHash = await bcrypt.hash(newPassword, saltRounds);
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    try {
        await userModel.updateOne({ _id: userId }, { $set: { password: newPasswordHash } }, { upsert: true });
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json({ message: "OK" });
}

userController.resetPassword = async function (req, res) {
    let userId = req.params.id;

    let foundUser;
    try {
        foundUser = await userModel.findById(userId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    let password = req.body.password;
    let passwordHash;
    try {
        passwordHash = await bcrypt.hash(password, saltRounds);
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    try {
        await userModel.updateOne({ _id: userId }, { $set: { password: passwordHash } }, { upsert: true });
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    resetPasswordMail(foundUser.email, password);
    res.status(200).json({ message: "OK" });
}

userController.getWatchList = async function (req, res) {
    let foundUser;
    try {
        let userId = req.params.id;
        foundUser = await userModel.findById(userId).populate({ path: "watchList", populate: { path: "highestBiddingEntry", populate: { path: "bidder" } } }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    // Incase some user documents don't have watchList object.
    res.status(200).json(foundUser.watchList ? foundUser.watchList : []);
}

userController.addWatchListItem = async function (req, res) {
    let userId = req.params.id;
    let productId = req.body.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundProduct) {
        res.status(404).json({ message: "No such product" });
        return;
    }

    let foundUser;
    try {
        foundUser = await userModel.findById(userId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    if (Array.isArray(foundUser.watchList)) {
        for (let element of foundUser.watchList) {
            if (element.equals(foundProduct._id)) {
                res.status(422).json({ message: "This product is already in place." });
                return;
            }
        }
    }

    try {
        foundUser = await userModel.findByIdAndUpdate(userId, { $addToSet: { watchList: productId } }, { new: true, select: "watchList" }).exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(foundUser);
}

userController.checkWatchListItem = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    // HEAD Method doesn't have body, just return status code.
    let foundUser
    try {
        foundUser = await userModel.findById(userId, "watchList").exec();
    } catch (error) {
        res.status(500).end();
        return;
    }

    if (!foundUser) {
        res.status(404).end();
        return;
    }

    let watchList = foundUser.watchList;
    if (!watchList || watchList.indexOf(productId) === -1) {
        res.status(404);
        return;
    }

    res.status(200).end();
}

userController.deleteWatchListItem = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundUser;
    try {
        foundUser = await userModel.findByIdAndUpdate(userId, { $pull: { watchList: productId } }, { upsert: true, new: true }).exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json({ message: "OK" });
}

userController.listBiddingProduct = async function (req, res) {
    let userId = req.params.id;

    let productIds;
    try {
        productIds = await currentEntryModel.find().distinct("product").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    try {
        productIds = await biddingEntryModel.find({ product: { $in: productIds }, bidder: userId }).distinct("product").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let products;
    try {
        products = await productModel.find({ _id: { $in: productIds } }).populate({ path: "highestBiddingEntry" }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(products);
}

userController.listWinningProduct = async function (req, res) {
    let userId = req.params.id;

    let products;
    try {
        products = await productModel.find({ winner: userId }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(products);
}

userController.listSellingProduct = async function (req, res) {
    let userId = req.params.id;

    let productIds;
    try {
        productIds = await currentEntryModel.find().distinct("product").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let products;
    try {
        products = await productModel.find({ _id: { $in: productIds }, seller: ObjectId(userId) }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json(products);
}

userController.listSoldProduct = async function (req, res) {
    let userId = req.params.id;

    let products;
    try {
        products = await productModel.find({ seller: userId, winner: { $exists: true, $ne: null } }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(products);
}

userController.listReview = async function (req, res) {
    let userId = req.params.id;

    // Nếu như làm Seller | Winner của một product thì sẽ có khả năng được review.
    let products;
    try {
        products = await productModel.find({ $or: [{ seller: userId }, { winner: userId }] }, "seller winner sellerReview winnerReview").sort({ endTime: "descending" }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    let user = ObjectId(userId);
    let reviews = [];
    for (let product of products) {
        if (product.seller && product.seller.equals(user) && product.sellerReview) {
            reviews.push(product.sellerReview);
        }

        if (product.winner && product.winner.equals(user) && product.winnerReview) {
            reviews.push(product.winnerReview);
        }
    }

    res.status(200).json(reviews);
}

userController.checkSellerReview = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).lean().exec();
    } catch (error) {
        res.status(500).end();
        return;
    }

    if (!foundProduct) {
        res.status(404).end();
        return;
    }

    if (!foundProduct.winner.equals(ObjectId(userId))) {
        res.status(404).end();
        return;
    }

    if (!foundProduct.sellerReview) {
        res.status(404).end();
        return;
    }

    res.status(200).end();
}

userController.checkWinnerReview = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).lean().exec();
    } catch (error) {
        res.status(500).end();
        return;
    }

    if (!foundProduct) {
        res.status(404).end();
        return;
    }

    if (!foundProduct.seller.equals(ObjectId(userId))) {
        res.status(404).end();
        return;
    }

    if (!foundProduct.winnerReview) {
        res.status(404).end();
        return;
    }

    res.status(200).end();
}

userController.getSellerReview = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundProduct) {
        res.status(404).json({ message: "No such product", type: 1 });
        return;
    }

    if (!foundProduct.sellerReview) {
        res.status(404).json({ message: "Winner has not reviewed yet", type: 2 });
        return;
    }

    res.status(200).json(foundProduct.sellerReview);
}

userController.getWinnerReview = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundProduct) {
        res.status(404).json({ message: "No such product", type: 1 });
        return;
    }

    if (!foundProduct.winnerReview) {
        res.status(404).json({ message: "Seller has not reviewed yet", type: 2 });
        return;
    }

    res.status(200).json(foundProduct.winnerReview);
}

userController.createSellerReview = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundProduct) {
        res.status(404).json({ message: "No such product", type: 1 });
        return;
    }

    if (!foundProduct.winner.equals(ObjectId(userId))) {
        res.status(404).json({ message: "This user is not winner", type: 2 });
        return;
    }

    if (foundProduct.sellerReview) {
        res.status(422).json({ message: "The review has been created" });
        return;
    }

    let data = {
        isOK: req.body.isOK,
        comment: req.body.comment,
        time: Date.now()
    };

    let pointUnit;
    if (data.isOK) {
        pointUnit = 1;
    } else {
        pointUnit = -1;
    }

    try {
        await productModel.findByIdAndUpdate(productId, { $set: { sellerReview: data } });
        await userModel.findByIdAndUpdate(foundProduct.seller, { $inc: { reviewPoint: pointUnit } });
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json({ message: "OK" });
}

userController.createWinnerReview = async function (req, res) {
    let userId = req.params.id;
    let productId = req.params.productId;

    let foundProduct;
    try {
        foundProduct = await productModel.findById(productId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (!foundProduct) {
        res.status(404).json({ message: "No such product", type: 1 });
        return;
    }

    if (!foundProduct.seller.equals(ObjectId(userId))) {
        res.status(404).json({ message: "This user is not seller", type: 2 });
        return;
    }

    if (!foundProduct.winner) {
        res.status(404).json({ message: "This product don't have winner", type: 3 });
        return;
    }

    if (foundProduct.winnerReview) {
        res.status(422).json({ message: "The review has been created" });
        return;
    }

    let data = {
        isOK: req.body.isOK,
        comment: req.body.comment,
        time: Date.now()
    };

    let pointUnit;
    if (data.isOK) {
        pointUnit = 1;
    } else {
        pointUnit = -1;
    }

    try {
        await productModel.findByIdAndUpdate(productId, { $set: { winnerReview: data } });
        await userModel.findByIdAndUpdate(foundProduct.winner, { $inc: { reviewPoint: pointUnit } });
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    res.status(200).json({ message: "OK" });
}

userController.getPublicProfile = async function (req, res) {
    let userId = req.params.id;

    // Dùng lại source code của getReview:
    let products;
    try {
        products = await productModel.find({ $or: [{ seller: userId }, { winner: userId }] }, "seller winner sellerReview winnerReview").sort({ endTime: "descending" }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    userId = ObjectId(userId);
    let reviews = [];
    for (let product of products) {
        if (product.seller && product.seller.equals(userId) && product.sellerReview) {
            reviews.push(product.sellerReview);
        }

        if (product.winner && product.winner.equals(userId) && product.winnerReview) {
            reviews.push(product.winnerReview);
        }
    }
    // ====

    let user;
    try {
        user = await userModel.findById(userId, "userName reviewPoint").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    let publicProfile = {
        userName: user.userName,
        reviewPoint: user.reviewPoint,
        reviews: reviews
    };

    res.status(200).json(publicProfile);
}

userController.createUpgradeRequest = async function (req, res) {
    let userId = req.params.id;

    let foundUser;
    try {
        foundUser = await userModel.findById(userId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (foundUser.roles.indexOf("seller") !== -1) {
        res.status(422).json({ message: "This user is a seller already", type: 1 });
        return;
    }

    if (foundUser.upgradeRequest) {
        res.status(422).json({ message: "Request is already exists", type: 2 });
        return;
    }

    try {
        foundUser = await userModel.findByIdAndUpdate(userId, { $set: { upgradeRequest: { time: Date.now() } } }, { upsert: true, new: true }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(foundUser);
};

userController.listUpgradeRequest = async function (req, res) {
    let foundUsersHaveUpgradeRequest;

    try {
        foundUsersHaveUpgradeRequest = await userModel.find({ upgradeRequest: { $exists: true } }, "userName upgradeRequest").sort({ "upgradeRequest.time": "descending" }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(foundUsersHaveUpgradeRequest);
}

userController.deleteUpgradeRequest = async function (req, res) {
    let userId = req.params.userId;

    let foundUser;
    try {
        foundUser = await userModel.findById(userId, "userName upgradeRequest").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user", type: 1 });
        return;
    }

    if (!foundUser.upgradeRequest) {
        res.status(404).json({ message: "Upgrade request is not exists", type: 2 });
        return;
    }

    try {
        foundUser = await userModel.findByIdAndUpdate(userId, { $unset: { upgradeRequest: "" } }, { new: true, select: "userName upgradeRequest" }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(foundUser);
}

userController.acceptUpgradeRequest = async function (req, res) {
    let userId = req.params.userId;

    let foundUser;
    try {
        foundUser = await userModel.findById(userId, "userName upgradeRequest").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user", type: 1 });
        return;
    }

    if (!foundUser.upgradeRequest) {
        res.status(404).json({ message: "Upgrade request is not exists", type: 2 });
        return;
    }

    try {
        foundUser = await userModel.findByIdAndUpdate(userId, { $addToSet: { roles: "seller" }, $unset: { upgradeRequest: "" } }, { new: true }).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(foundUser);
}

async function resetPasswordMail(userEmail, password) {
    let resetPasswordMailOptions = {
        from: "get.high.auction@example.com",
        to: userEmail,
        subject: "Reset password",
        text: `Mật khẩu mới: ${password}`
    };

    await transporter.sendMail(resetPasswordMailOptions);
}

module.exports = userController;
