const categoryModel = require("../models/category");

const categoryController = {};

categoryController.list = async (req, res) => {
    let categories;
    try {
        categories = await categoryModel.find({}).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Error when getting category list" });
        return;
    }

    res.status(200).json(categories);
};

categoryController.show = async (req, res) => {
    let id = req.params.id;

    let category;
    try {
        category = await categoryModel.findById(id).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Error when getting category list" });
        return;
    }

    if (!category) {
        res.status(404).json({ message: "No such category" });
        return;
    }

    res.status(200).json(category);
};

categoryController.create = async (req, res) => {
    let document = {
        name: req.body.name
    };

    let createdCategory;
    try {
        createdCategory = await categoryModel.create(document);
    } catch (error) {
        if (error.code === 11000) {
            res.status(422).json("Duplicate category name.");
            return;
        }

        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    res.status(200).json(createdCategory);
};

categoryController.update = async (req, res) => {
    let categoryId = req.params.id;
    let document = {
        name: req.body.name
    };

    let foundCategory;
    try {
        foundCategory = await categoryModel.findByIdAndUpdate(categoryId, { $set: document }, { upsert: true, new: true }).lean().exec();
    } catch (error) {
        if (error.code === 11000) {
            res.status(422).json({ message: "Duplicate category name" });
            return;
        }

        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundCategory) {
        res.status(404).json({ message: "No such category" });
        return;
    }

    res.status(200).json(foundCategory);
};

categoryController.delete = (req, res) => {
    let categoryId = req.params.id;

    let deletedCategory;
    try {
        deletedCategory = categoryModel.findByIdAndRemove(categoryId).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!deletedCategory) {
        res.status(404).json({ message: "No such category" });
    }

    res.status(200).json(deletedCategory);
};

module.exports = categoryController;
