const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");

const checkJWT = require("./middlewares/checkJWT.js");
const hasRoles = require("./middlewares/hasRoles");

function isSelf(req, res, next) {
    let userId = req.tokenPayload.userId;
    let userRoles = req.tokenPayload.userRoles;
    let requestedId = req.params.id;

    let isAdmin = (userRoles.indexOf("admin") !== -1);

    if (userId !== requestedId && !isAdmin) {
        res.status(403).json({ message: "Don't have permission" });
        return;
    }

    next();
}

router.get("/", checkJWT, hasRoles("admin"), userController.list);

router.get("/:id", checkJWT, hasRoles("bidder"), isSelf, userController.show);

router.post("/", checkJWT, hasRoles("admin"), userController.create);

router.put("/:id", checkJWT, hasRoles("bidder"), isSelf, userController.update);

router.delete("/:id", checkJWT, hasRoles("admin"), isSelf, userController.remove);

router.post("/:id/password", checkJWT, hasRoles("bidder"), isSelf, userController.changePassword);

router.post("/:id/password-reset", checkJWT, hasRoles("admin"), userController.resetPassword);

router.get("/:id/watchlist", checkJWT, hasRoles("bidder"), isSelf, userController.getWatchList);

router.post("/:id/watchlist", checkJWT, hasRoles("bidder"), isSelf, userController.addWatchListItem);

router.head("/:id/watchlist/:productId", checkJWT, hasRoles("bidder"), isSelf, userController.checkWatchListItem);

router.delete("/:id/watchlist/:productId", checkJWT, hasRoles("bidder"), isSelf, userController.deleteWatchListItem);

router.get("/:id/biddingproduct", checkJWT, hasRoles("bidder"), isSelf, userController.listBiddingProduct);

router.get("/:id/winningproduct", checkJWT, hasRoles("bidder"), isSelf, userController.listWinningProduct);

router.get("/:id/sellingproduct", checkJWT, hasRoles("seller"), isSelf, userController.listSellingProduct);

router.get("/:id/soldproduct", checkJWT, hasRoles("seller"), isSelf, userController.listSoldProduct);

// Review
router.get("/:id/review", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.listReview);

router.head("/:id/review/:productId/seller", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.checkSellerReview);

router.head("/:id/review/:productId/winner", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.checkWinnerReview);

router.get("/:id/review/:productId/seller", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.getSellerReview);

router.get("/:id/review/:productId/winner", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.getWinnerReview);

router.post("/:id/review/:productId/seller", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.createSellerReview);

router.post("/:id/review/:productId/winner", checkJWT, hasRoles("bidder", "seller"), isSelf, userController.createWinnerReview);
// ====

router.get("/:id/public-profile", userController.getPublicProfile);

router.post("/:id/upgrade-request", checkJWT, isSelf, userController.createUpgradeRequest);

module.exports = router;
