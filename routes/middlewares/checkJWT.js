const jwt = require("jsonwebtoken");
const config = require("../../config.js");

const jwtSecretKey = config.get("jwtSecretKey");

const checkJWT = function(req, res, next) {
    let accessToken = req.headers["x-access-token"];
    if (!accessToken) {
        res.status(403).json({
            message: "No token found"
        });
        return;
    }

    jwt.verify(accessToken, jwtSecretKey, (error, payload) => {
        if (error) {
            res.status(401).json({
                message: "Verify failed",
                error: error
            });
            return;
        }
        // Sử dụng thông tin chứa trong token cho những thao tác kế tiếp (next middleware hoặc request handler).
        // Bằng cách truy cập thuộc tính tokenPayload.
        req.tokenPayload = payload;
        next();
    });
};

module.exports = checkJWT;
