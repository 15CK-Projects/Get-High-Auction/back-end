const hasRoles = function (...allowed) {
    return function (req, res, next) {
        let tokenPayload = req.tokenPayload;
        if (!tokenPayload) {
            res.status(403).json({
                message: "Token not found"
            });
            return;
        }

        let userRoles = tokenPayload.userRoles;
        userRoles = userRoles.filter(role => allowed.indexOf(role) !== -1);
        if (userRoles.length === 0) {
            res.status(403).json({
                message: "Not allowed"
            });
            return;
        }

        next();
    }
};

module.exports = hasRoles;
