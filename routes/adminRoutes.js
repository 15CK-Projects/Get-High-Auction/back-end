const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");

const checkJWT = require("./middlewares/checkJWT.js");
const hasRoles = require("./middlewares/hasRoles");

router.use(checkJWT);
router.use(hasRoles("admin"));

router.get("/upgrade-request", userController.listUpgradeRequest);
router.put("/upgrade-request/:userId", userController.acceptUpgradeRequest);
router.delete("/upgrade-request/:userId", userController.deleteUpgradeRequest);

module.exports = router;
