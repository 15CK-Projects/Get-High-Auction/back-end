const app = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');

const router = app.Router();
const config = require("../config.js");
const secretKey = config.get("jwtSecretKey");
const crypto = require("crypto");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const axios = require("axios");
const queryString = require("querystring");

const userModel = require('../models/user');

router.route("/login").post(async (req, res) => {
    let userName = req.body.userName;
    let password = req.body.password;
    if (!userName || !password) {
        res.status(400).json({ message: "Missing user name or password" });
        return;
    }

    let foundUser;
    try {
        foundUser = await userModel.findOne({ userName: userName }, "_id userName password").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    if (foundUser.isDeleted) {
        res.status(404).json({ message: "User is deleted" });
        return;
    }

    let isPasswordOK;
    try {
        isPasswordOK = await bcrypt.compare(password, foundUser.password);
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    if (!isPasswordOK) {
        res.status(401).json({ message: "Wrong password" });
        return;
    }

    let refreshToken = crypto.randomBytes(64).toString("hex");

    let userId = foundUser._id; // For reusing when return data to client.
    try {
        foundUser = await userModel.findByIdAndUpdate(userId,
            { $set: { refreshToken: refreshToken } },
            { new: true, upsert: true, select: "-_id userName roles" }
        ).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error });
        return;
    }

    let payload = {
        userId: userId,
        userRoles: foundUser.roles
    };

    let accessToken = jwt.sign(payload, secretKey, {
        expiresIn: "1h"
    });

    res.status(200).json({
        userId: userId,
        accessToken: accessToken,
        expiredAt: jwt.decode(accessToken, { complete: true }).payload.exp * 1000, // Change second to milisecond.
        refreshToken: refreshToken,
        user: foundUser
    });
});

router.route("/register").post(async (req, res) => {
    let user = req.body;

    let parameters = queryString.stringify({
        secret: "6LdL4D8UAAAAALFz1qpupKxSoHcAovthGQrm6ptO",
        response: user["g-recaptcha-response"]
    });

    let response;
    try {
        response = await axios({
            url: "https://www.google.com/recaptcha/api/siteverify",
            method: "post",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: parameters,
            responseType: "json",
            responseEncoding: "utf8"
        });
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    if (response.data.success === false) {
        res.status(400).json({ message: "Invalid Google reCAPTCHA" });
        return;
    }

    let refreshToken = crypto.randomBytes(64).toString("hex");

    user.refreshToken = refreshToken;
    user.roles = ["bidder"];

    let passwordHash;
    try {
        passwordHash = await bcrypt.hash(user.password, saltRounds);
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    user.password = passwordHash; // Replace plain password with bcrypt encrypted hash.

    try {
        user = await userModel.create(user);
    } catch (error) {
        res.status(500).json({ message: "Internal server error" });
        return;
    }

    let payload = {
        userId: user._id,
        userRoles: user.roles
    };

    let accessToken = jwt.sign(payload, secretKey, {
        expiresIn: "1h"
    });

    res.status(200).json({
        userId: user._id,
        accessToken: accessToken,
        expiredAt: jwt.decode(accessToken, { complete: true }).payload.exp * 1000,
        refreshToken: refreshToken,
        user: {
            userName: user.userName,
            roles: user.roles
        }
    });
});

router.route("/token").post(async (req, res) => {
    let userId = req.body.userId;
    let refreshToken = req.body.refreshToken;

    if (!userId || !refreshToken) {
        res.status(400).json({ message: "Missing user identification or refresh token" });
        return;
    }

    let foundUser;
    try {
        foundUser = await userModel.findById(userId, "_id refreshToken").lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal error server", error: error });
        return;
    }

    if (!foundUser) {
        res.status(404).json({ message: "No such user" });
        return;
    }

    if (foundUser.isDeleted) {
        res.status(404).json({ message: "User is deleted" });
        return;
    }

    if (foundUser.refreshToken !== refreshToken) {
        res.status(401).json({ message: "Wrong refresh token" });
        return;
    }

    refreshToken = crypto.randomBytes(64).toString("hex");

    try {
        foundUser = await userModel.findByIdAndUpdate(userId,
            { $set: { refreshToken: refreshToken } },
            { upsert: true, new: true, select: "-_id userName roles" }
        ).lean().exec();
    } catch (error) {
        res.status(500).json({ message: "Internal error server", error: error });
        return;
    }

    let payload = {
        userId: userId,
        userRoles: foundUser.roles
    };

    let accessToken = jwt.sign(payload, secretKey, {
        expiresIn: "1h"
    });

    res.status(200).json({
        userId: userId,
        accessToken: accessToken,
        expiredAt: jwt.decode(accessToken, { complete: true }).payload.exp * 1000,
        refreshToken: refreshToken,
        user: foundUser
    });
});

module.exports = router;
