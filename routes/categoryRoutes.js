const app = require("express");
const router = app.Router();
const categoryController = require("../controllers/categoryController.js");

const checkJWT = require("./middlewares/checkJWT.js");
const hasRoles = require("./middlewares/hasRoles.js");

router.get("/", categoryController.list);

router.get("/:id", categoryController.show);

router.post("/", checkJWT, hasRoles("admin"),categoryController.create);

router.put("/:id", checkJWT, hasRoles("admin"), categoryController.update);

router.delete("/:id", checkJWT, hasRoles("admin"), categoryController.delete);

module.exports = router;
