const express = require("express");
const productRouter = express.Router();

const productController = require("../controllers/productController.js");

const checkJWT = require("./middlewares/checkJWT.js");
const hasRoles = require("./middlewares/hasRoles.js");

const multer = require("multer");

function getExtension(path) {
    let basename = path.split(/[\\/]/).pop();
    let pos = basename.lastIndexOf(".");

    if (basename === "" || pos < 1) {
        return "";
    }

    return basename.slice(pos + 1);
}

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "./public/images");
    },
    filename: function (req, file, cb) {
        let fileName = file.fieldname + Date.now() + "." + getExtension(file.originalname);
        cb(null, fileName);
    }
});

let upload = multer({ storage: storage });

productRouter.get("/", productController.list);

productRouter.get("/:id", productController.show);

productRouter.post("/", checkJWT, hasRoles("seller", "admin"), upload.array("images", 3), productController.create);

productRouter.put("/:id", checkJWT, hasRoles("seller", "admin"), productController.update);

productRouter.delete("/:id", checkJWT, hasRoles("admin"), productController.remove);

// == Bidding routes ==
productRouter.get("/:id/bidding-entry", checkJWT, hasRoles("bidder"), productController.listBiddingEntry);

productRouter.post("/:id/bidding-entry", checkJWT, hasRoles("bidder"), productController.addBidding);

productRouter.post("/:id/description", checkJWT, hasRoles("seller"), productController.addDescription);
// ====

productRouter.post("/:id/ban", checkJWT, hasRoles("bidder"), productController.banHighestBidder);

module.exports = productRouter;
