const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");

require("./automation/agenda.js"); // Agenda initialize.
const config = require("./config.js");

const path = require("path");

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(morgan("combined"));

const authRoutes = require("./routes/authRoutes.js");
const userRoutes = require("./routes/userRoutes.js");
const adminRoutes = require("./routes/adminRoutes.js");
const categoryRoutes = require("./routes/categoryRoutes.js");
const productRoutes = require("./routes/productRoutes.js");

app.use("/auth", authRoutes);
app.use("/user", userRoutes);
app.use("/category", categoryRoutes);
app.use("/product", productRoutes);
app.use("/admin", adminRoutes);

app.use("/public", express.static(path.join(__dirname, "public")));

const mongoDB = `mongodb://${config.get("db.host")}/${config.get("db.database")}`;
mongoose.Promise = global.Promise;

mongoose.connect(mongoDB, {
    user: config.get("db.userName"),
    pass: config.get("db.password")
}).then(() => {
    // Starting api when moongose's connection is ready.
    app.listen(config.get("port"), () => {
        console.log(`Server is listening on port ${config.get("port")} ...`);
    });
});

const connection = mongoose.connection;
connection.on('error', console.error.bind(console, "MongoDB connection error:"));
