const Agenda = require("agenda");
const config = require("../config.js");
const productModel = require("../models/product.js");
const biddingEntryModel = require("../models/biddingEntry.js");
const currentEntryModel = require("../models/currentEntry.js");

const mongoose = require("mongoose");

const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'phwmbfk44s7gu6ww@ethereal.email',
        pass: 'uQxFrNSCBTMmVtVrnA'
    }
});

let agenda = new Agenda()
    .processEvery("1 seconds")
    .mongo(mongoose.connection, "agendaJobs");

agenda.on("error", () => {
    console.error("Agenda can't connect to MongoDB Server! Please shutdown the Node.js Server now!");
});

// Tự động chạy (wired) khi Mongoose kết nối thành công.
agenda.on("ready", () => {
    agenda.start();
    console.log("Agenda is running ...");
    agenda.now("ClearOutOfDateCurrentEntry");
});

// Xử lí currentEntry (trong currentEntries collection), hay sản phẩm đã hết thời gian đấu giá.
agenda.define("DeleteOutOfDateCurrentEntry", async function (job, done) {
    try {
        let entry = await currentEntryModel.findOne({ product: job.attrs.data.product }).populate({ path: "product" }).lean().exec();

        if (entry) {
            let product = await productModel.findById(entry.product._id).populate({ path: "highestBiddingEntry" }).lean().exec();

            let now = new Date();
            if (product && product.endTime > now) {
                job.schedule(product.endTime);
                job.save();
            } else {
                if (product && product.highestBiddingEntry) {
                    product = await productModel.findByIdAndUpdate(product._id, { $set: { winner: product.highestBiddingEntry.bidder } }).lean().exec();
                }
                await currentEntryModel.findByIdAndRemove(entry._id);
                endAuctionMail(product._id);
                job.remove();
            }
        } else {
            job.remove();
        }
    } catch (error) {
        console.log(error);
    }

    done();
});

agenda.define("ClearOutOfDateCurrentEntry", async function (job, done) {
    /*
    try {
        let entries = await currentEntryModel.find({})
            .populate({ path: "product" })
            .lean().exec();

        let now = Date.now();
        let outOfDateEntries = entries.filter(entry => entry.product.endTime <= now);

        for (let entry of outOfDateEntries) {
            let product = await productModel.findById(entry.product._id).populate({ path: "highestBiddingEntry" }).lean().exec();

            if (product && product.highestBiddingEntry) {
                await productModel.findByIdAndUpdate(product._id, { $set: { winner: product.highestBiddingEntry.bidder } }).lean().exec();
            }

            await currentEntryModel.findByIdAndRemove(entry._id);
        }

        // TODO: Clear relative jobs.
    } catch (error) {
        console.log();
    }
    */
    job.remove();
    done();
});

agenda.define("AutoBidding", async function (job, done) {
    let product = job.attrs.data.product; // Product _id

    // Kiểm tra product tồn tại hay không.
    let foundProduct;
    try {
        foundProduct = await productModel.findById(product).populate({ path: "highestBiddingEntry" }).lean().exec();
    }
    catch (error) {
        console.error(error);
        done();
        return;
    }

    if (!product) {
        done();
        return;
    }
    // ====

    let oldBiddingEntryId = foundProduct.highestBiddingEntry ? foundProduct.highestBiddingEntry._id : null;

    // Kiểm tra product còn được đấu giá hay không.
    let foundCurrentEntry;
    try {
        foundCurrentEntry = await currentEntryModel.findOne({ product: product }).lean().exec();
        if (!foundCurrentEntry) {
            throw "Product is out of date.";
        }

        if (!foundCurrentEntry.pendingBiddings) {
            throw "Pending bidding list is null.";
        }
    }
    catch (error) {
        console.error(error);
        done();
        return;
    }
    // ====

    // Tiến hành đấu giá.
    do {
        let deleted = []; // Mảng đánh dấu những pending bidding entry bị loại.

        // Loại những pending bidding nhỏ hơn hoặc bằng hơn giá đang giữ hiện tại.
        let lowerBoundPrice = foundProduct.highestBiddingEntry ? foundProduct.highestBiddingEntry.price : foundProduct.startPrice;
        foundCurrentEntry.pendingBiddings = foundCurrentEntry.pendingBiddings.filter((pendingBidding) => {
            if (pendingBidding.price <= lowerBoundPrice) {
                deleted.push(pendingBidding._id);
                return false;
            }
            return true;
        });
        // ====

        let createdBiddingEntry;
        let biddingCountIncrement = 0;
        let final = false;

        if (foundCurrentEntry.pendingBiddings.length === 0) {
            break;
        } else if (foundCurrentEntry.pendingBiddings.length === 1) {
            let pendingBidding = foundCurrentEntry.pendingBiddings[0];
            let price = lowerBoundPrice + foundProduct.priceStep;
            // Giá bidder đưa ra không đủ khi so với (giá cũ + bước giá).
            // Trường hợp này không thể xảy ra nếu xét điều kiện lúc tạo pending bidding. Trừ khi database bị đổi hoặc bước giá bị đổi.
            if (pendingBidding.price < price) {
                deleted.push(pendingBidding._id);
            } else {
                try {
                    createdBiddingEntry = await biddingEntryModel.create({
                        product: foundProduct._id,
                        bidder: pendingBidding.bidder,
                        price: price
                    });

                    if (pendingBidding.price === price || pendingBidding.price < price + foundProduct.priceStep) {
                        deleted.push(pendingBidding._id);
                    }

                    biddingCountIncrement += 1;
                    // Kết thúc, do hiện tại không còn ai cạnh tranh.
                    final = true;
                } catch (error) {
                    console.error(error);
                }
            }
        } else {
            // Xử lí cho quá trình đấu giá tự động, chỉ xét khi trong pending biddings có từ 2 entries trở lên.
            // Lúc này dùng giá max để đấu với nhau cho đến khi chỉ còn lại một pending bidding có mức giá lớn nhất.

            // Sắp xếp pending bidding theo giá đặt.
            foundCurrentEntry.pendingBiddings.sort((a, b) => {
                if (a.price > b.price) {
                    return 1;
                }

                if (a.price < b.price) {
                    return -1;
                }

                return 0;
            });
            // ====

            // Lấy giá đặt nhỏ nhất.
            let minPendingBiddingPrice = foundCurrentEntry.pendingBiddings[0].price;
            let minPendingBiddings = foundCurrentEntry.pendingBiddings.filter((pendingBidding) => {
                if (pendingBidding.price !== minPendingBiddingPrice) {
                    return false;
                }
                return true;
            });
            // ====

            // Phòng ngừa việc price step bị đổi.
            if (minPendingBiddingPrice % foundProduct.priceStep !== 0) {
                for (minPendingBidding of minPendingBiddings) {
                    deleted.push(minPendingBidding._id);
                }
            } else {
                minPendingBiddings.sort((a, b) => {
                    // In case same price, compare time.
                    if (a.time > b.time) {
                        return -1;
                    }

                    if (a.time < b.time) {
                        return 1;
                    }

                    // In case same price and same time, very very rare.
                    // TODO: User who registered earlier has higher priority.
                });

                // Những ai trùng giá đặt thì ưu tiên người đặt sớm nhất.
                // (bidding entry cuối cùng được chỉ định làm highest bidding entry cho product, thuộc về bidder đặt giá sớm nhất)
                // Tuy nhiên vẫn ghi nhận lại trong biddingEntry collection các bidding entry cùng giá nhưng trễ hơn.
                for (minPendingBidding of minPendingBiddings) {
                    try {
                        // Create a new bidding entry (logging bidder's bidding).
                        createdBiddingEntry = await biddingEntryModel.create({
                            product: foundCurrentEntry.product,
                            bidder: minPendingBidding.bidder,
                            price: minPendingBidding.price
                        });

                        biddingCountIncrement += 1;
                    } catch (error) {
                        console.log(error);
                    }

                    // Bidder đã phải dùng giá max để đấu.
                    deleted.push(minPendingBidding._id);
                }

                // Kết thúc, do hiện tại không còn ai cạnh tranh.
                if (minPendingBiddings.length === foundCurrentEntry.pendingBiddings.length) {
                    final = true;
                }
            }
        }

        try {
            // Xóa những pending bidding entry bị đánh dấu.
            foundCurrentEntry = await currentEntryModel.findByIdAndUpdate(foundCurrentEntry._id,
                {
                    $pull: { pendingBiddings: { _id: { $in: deleted } } }
                },
                { upsert: true, new: true }
            ).lean().exec();
        } catch (error) {
            console.error(error);
        }

        if (createdBiddingEntry) {
            try {
                // Replace foundProduct.highestBiddingEntry with the new one.
                foundProduct = await productModel.findByIdAndUpdate(foundProduct._id,
                    {
                        $set: { highestBiddingEntry: createdBiddingEntry._id },
                        $inc: { biddingCount: biddingCountIncrement }
                    },
                    { upsert: true, new: true }
                ).populate({ path: "highestBiddingEntry" }).lean().exec();
            } catch (error) {
                console.error(error);
            }

            if (final) {
                // Gửi mail
                successBiddingMail(oldBiddingEntryId, createdBiddingEntry._id, foundProduct._id);
                console.log("Gửi email");
                break; // Kết thúc đấu giá tự động.
            }
        } else {
            try {
                // Bỏ đoạn này.
                foundProduct = await findById(product).populate({ path: "highestBiddingEntry" }).lean().exec();
            } catch (error) {
                console.error(error);
            }
        }

    } while (foundCurrentEntry.pendingBiddings.length > 0);
    // ====

    job.remove();
    done();
});

async function endAuctionMail(productId) {
    try {
        let product;
        product = await productModel.findById(productId)
            .populate({ path: "seller" })
            .populate({ path: "winner" })
            .lean().exec();

        if (!product) {
            return;
        }

        if (product.winner) {
            let winner = product.winner;
            let winnerMailOptions = {
                from: "get.high.auction@email.com",
                to: winner.email,
                subject: "Kết thúc đấu giá: Người thắng",
                text: `Chúc mừng bạn đã thắng đấu giá cho sản phẩm: ${product.name}`
            }

            await transporter.sendMail(winnerMailOptions);
        }

        if (product.seller) {
            let seller = product.seller;
            let sellerMailOptions = {
                from: "get.high.auction@email.com",
                to: seller.email,
                subject: "Kết thúc đấu giá: Người bán",
                text: `Sản phẩm "${product.name}" đã kết thúc đấu giá.`
            }

            await transporter.sendMail(sellerMailOptions);
        }
    } catch (error) {
        console.log(error);
    }
}

async function successBiddingMail(oldBiddingEntryId, newBiddingEntryId, productId) {
    try {
        let product = await productModel.findById(productId).populate({ path: "seller" }).lean().exec();

        let oldBiddingEntry = await biddingEntryModel.findById(oldBiddingEntryId).populate({ path: "bidder" }).lean().exec();
        let newBiddingEntry = await biddingEntryModel.findById(newBiddingEntryId).populate({ path: "bidder" }).lean().exec();

        if (oldBiddingEntry
            && !oldBiddingEntry.bidder._id.equals(newBiddingEntry.bidder._id)) {
            let oldMailOptions = {
                from: "get.high.auction@example.com",
                to: oldBiddingEntry.bidder.email,
                subject: "Giá sản phẩm thay đổi: Mất vị trí dẫn đầu",
                text: `Bạn đã mất bị ví dẫn đầu cho sản phẩm: ${product.name}`
            };

            await transporter.sendMail(oldMailOptions);
        }

        if (newBiddingEntry) {
            let newMailOptions = {
                from: "get.high.auction@example.com",
                to: newBiddingEntry.bidder.email,
                subject: "Giá sản phẩm thay đổi: Đặt giá thành công",
                text: `Bạn đã đặt giá thành công cho sản phẩm: ${product.name}`
            };

            await transporter.sendMail(newMailOptions);
        }

        let sellerMailOptions = {
            from: "get.high.auction@example.com",
            to: product.seller.email,
            subject: "Giá sản phẩm thay đổi: Người bán",
            text: `Giá của sản phẩm "${product.name}" đã thay đổi.`
        }

        await transporter.sendMail(sellerMailOptions);

    } catch (error) {
        console.log(error);
    }
}
