const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let biddingEntrySchema = new Schema({
    product: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true,
        ref: "Product"
    },
    bidder: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true,
        ref: "User"
    },
    price: {
        type: Number,
        required: true
    },
    time: {
        type: Date,
        required: true,
        default: Date.now
    }
});

module.exports = mongoose.model("BiddingEntry", biddingEntrySchema, "biddingEntries");
