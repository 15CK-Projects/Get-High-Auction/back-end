const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let productSchema = new Schema({
    name: {
        type: String,
        required: true,
        index: true
    },
    category: {
        type: Schema.Types.ObjectId
    },
    images: [{
        type: String
    }],
    description: {
        type: [{
            content: String,
            time: Date
        }]
    },
    // ======================================================
    seller: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true,
        ref: "User"
    },
    startPrice: {
        type: Schema.Types.Number,
        required: true
    },
    priceStep: {
        type: Number,
        required: true,
    },
    takeoverPrice: {
        type: Schema.Types.Number,
    },
    startTime: {
        type: Date,
        default: Date.now
    },
    endTime: {
        type: Date,
        default: () => Date.now() + 7 * 24 * 60 * 60 * 1000
    },
    timeExtension: {
        type: Boolean
    },
    highestBiddingEntry: {
        type: Schema.Types.ObjectId,
        ref: "BiddingEntry"
    },
    biddingCount: {
        type: Number,
        default: 0
    },
    winner: {
        type: Schema.Types.ObjectId,
        ref: "User",
        index: true
    },
    sellerReview: {
        isOK: {
            type: Boolean
        },
        comment: {
            type: String
        },
        time: {
            type: Date
        }
    },
    winnerReview: {
        isOK: {
            type: Boolean
        },
        comment: {
            type: String
        },
        time: {
            type: Date
        }
    }
});

// Text index.
productSchema.index({ name: "text" }, { default_language: "none" });

let productModel = mongoose.model("Product", productSchema, "products");

module.exports = productModel;
