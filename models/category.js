const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let categorySchema = new Schema({
    name: {
        type: String,
        index: true,
        unique: true
    },
    description: {
        type: String
    }
});

module.exports = mongoose.model("Category", categorySchema, "categories");
