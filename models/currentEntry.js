const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let currentEntrySchema = new Schema({
    product: {
        type: Schema.Types.ObjectId,
        required: true,
        index: true,
        ref: "Product"
    },
    pendingBiddings: {
        type: [{
            bidder: {
                type: Schema.Types.ObjectId,
                required: true,
                ref: "User"
            },
            price: {
                type: Number,
                required: true
            },
            time: {
                type: Date,
                required: true,
                default: Date.now
            }
        }],
        required: true,
        default: []
    },
    bannedBidders: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: "User"
        }],
        required: true,
        default: []
    }
});

module.exports = mongoose.model("CurrentEntry", currentEntrySchema, "currentEntries");
