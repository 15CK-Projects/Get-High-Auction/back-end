const mongoose = require("mongoose");
const Schema = mongoose.Schema; // Just create a shortcut.

let userSchema = new Schema({
    firstName: String,
    lastName: String,
    userName: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        default: null
    },
    email: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    birthday: Date,
    gender: {
        type: String,
        enum: ["female", "male"]
    },
    address: String,
    roles: {
        type: [{
            type: String,
            enum: ["admin", "seller", "bidder"]
        }],
        required: true,
        default: ["bidder"]
    },
    refreshToken: {
        type: String,
        index: true,
        unique: true
    },
    watchList: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: "Product"
        }],
        default: []
    },
    reviewPoint: {
        type: Number,
        default: null
    },
    upgradeRequest: {    
        time: {
            type: Date
        }
    },
    isDeleted: {
        type: Boolean
    }
});

module.exports = mongoose.model("User", userSchema, "users");
