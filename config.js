require("dotenv").config();
let convict = require("convict");

let config = convict({
    env: {
        doc: "The application environment.",
        format: ["production", "development", "test"],
        default: "development",
        env: "NODE_ENV"
    },
    port: {
        doc: "The port to bind.",
        format: "port",
        default: 3000,
        arg: "port",
        env: "APP_PORT"
    },
    jwtSecretKey: {
        doc: "JSON Web Token secret key",
        format: "*",
        default: "This_Is_The_Secret_Key",

    },
    db: {
        host: {
            doc: "MongoDB host name/IP",
            format: "*",
            default: "localhost",
        },
        port: {
            doc: "MongoDB port",
            format: "port",
            default: 27017
        },
        userName: {
            doc: "MongoDB user",
            format: "*",
            default: ""
        },
        password: {
            doc: "MongoDB password",
            format: "*",
            default: ""
        },
        database: {
            doc: "MongoDB database",
            format: "*",
            default: "get_high_auction"
        }
    }
});

let env = config.get("env");
config.loadFile(`./config/${env}.json`)

// Perform validation
config.validate({allowed: "strict"});

module.exports = config;
